#!/usr/bin/env python
import sys
import numpy as np 
from matplotlib import pyplot as plt 
import lecroyparser
import ROOT as r
from array import array
from pathlib import Path

def parse(fout,flist):
    print ("saving ttree in %s"% fout)

    #prepare ttree
    ftree=r.TFile(fout,"RECREATE")
    tree = r.TTree("waveforms","LeCroy Waveforms")
    time=array('d',1002*[0])
    c1=array('d',1002*[0])
    c1max=array('d',[0])
    c2=array('d',1002*[0])
    c2min=array('d',[0])
    c3=array('d',[0])
    c4=array('d',[0])
    tree.Branch("t",   time,"t[1002]/D")
    tree.Branch("pix", c1,  "pix[1002]/D")
    tree.Branch("pix_max", c1,  "pix_max/D")
    tree.Branch("sipm",c2,  "sipm[1002]/D")
    tree.Branch("sipm_min",c2,  "sipm_min/D")
    tree.Branch("xpos",c3,  "xpos/D")
    tree.Branch("ypos",c4,  "ypos/D")
    #loop over files
    savefig=False
    for path in flist:
        if (not Path(path).is_file() ):
            print ("file %s not found"% path)
            continue
        #parse lecroy data
        data = lecroyparser.ScopeData(path, parseAll = True)
        #save example plots
        if (savefig):
            saveplot(plt,data)
        #assign array branches
        for i in range(0,1001):
            time[i]=data.x[i]
            #print (time[i]," ",c1[i])
            c1[i]=data.y[0][i]
            c2[i]=data.y[1][i]
        #assign scalar branches 
        c1max[0]=max(data.y[0])
        c2min[0]=min(data.y[1])
        c3[0]=sum(data.y[2])/len(data.y[2])
        c4[0]=sum(data.y[3])/len(data.y[3])
        #fill tree
        tree.Fill()
    #save tree and close file
    tree.Write()
    ftree.Close()


def saveplot(plt):
    plt.clf()
    plt.plot(data.x,data.y[0])
    plt.plot(data.x,data.y[1])
    plt.savefig("pixel_sipm.png")
    plt.show()
    plt.clf()
    plt.plot(data.x,data.y[2])
    plt.plot(data.x,data.y[3])
    plt.savefig("xpos_ypos.png")
    plt.clf()

if __name__ == "__main__":
    #parse command line
    flist =[]
    fout = "ftree.root"
    if (len(sys.argv)>1):
        fout = sys.argv[1]
        if (len(sys.argv)>2):
            flist = sys.argv[2:]
    if (len(flist)<1):
        print ("no file list given")
        quit()
    parse(fout,flist)
