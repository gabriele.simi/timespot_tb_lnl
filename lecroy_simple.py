#!/usr/bin/env python
import sys
import numpy as np 
from matplotlib import pyplot as plt 
import lecroyparser
import ROOT as r
from array import array

#parse command line
flist =[]
if ( len(sys.argv)>1):
    flist = sys.argv[1:]
else:
    flist = ["../data_tb_Timespot_may2021/20210521/run_004/C4--Trace--03271.trc"]
#configure ttre
ftree=r.TFile("ftree.root","RECREATE")
tree = r.TTree("waveforms","LeCroy Waveforms")
branch_val={}
branch_names=['C1','C2','C3','C4','t']
for branch_name in branch_names:
    branch_val[branch_name]=array('d',1002*[0])
    tree.Branch(branch_name,branch_val[branch_name],branch_name+"[1002]/D")
#loop on file list
for path in flist:
    data = lecroyparser.ScopeData(path, parseAll = True)
    for i in range(0,len(data.x)):
        for ch in range(len(data.y)):
            branch_val[branch_names[ch]][i]=data.y[ch][i]
        branch_val['t'][i]=data.x[i]
    tree.Fill()
tree.Write()
ftree.Close()
    
