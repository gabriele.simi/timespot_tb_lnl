#!/usr/bin/env python
import sys
import numpy as np 
from matplotlib import pyplot as plt 
import lecroyparser
import ROOT as r
from array import array
from pathlib import Path

def parse(fout,flist):
    print ("saving ttree in %s"% fout)

    #prepare ttree
    ftree=r.TFile(fout,"RECREATE")
    tree = r.TTree("waveforms","LeCroy Waveforms")
    #time=array('d',1002*[0])
    #c1=array('d',1002*[0])
    #c2=array('d',1002*[0])
    time=array('d',2000*[0])
    c1=array('d',2000*[0])
    c2=array('d',2000*[0])

    #tree.Branch("t",   time,"t[1002]/D")
    #tree.Branch("sipm",c1,  "sipm[1002]/D")
    #tree.Branch("laser",c2,  "laser[1002]/D")
    
    tree.Branch("t",   time,"t[2000]/D")
    tree.Branch("sipm",c1,  "sipm[2000]/D")
    tree.Branch("laser",c2,  "laser[2000]/D")
    
   
    #loop over files
    savefig=False
    for path in flist:
        if (not Path(path).is_file() ):
            print ("file %s not found"% path)
            continue
        #parse lecroy data
        data = lecroyparser.ScopeData(path, parseAll = True)
        #save example plots
        if (savefig):
            saveplot(plt,data)
        #assign array branches
        #for i in range(0,1001):
        for i in range(0,1999):
            time[i]=data.x[i]
            #print (time[i]," ",c1[i])
            #print (time[i]," ",c2[i])
            c1[i]=data.y[0][i]
            c2[i]=data.y[1][i]
        
        #plt.clf()
        #plt.plot(data.x,data.y[0])
        #plt.plot(data.x,data.y[1]/1000)
        #plt.xlabel("t [s]")
        #plt.ylabel("Signals [V]")
        #plt.show()
        #plt.clf()
        
        #fill tree
        tree.Fill()
    #save tree and close file
    tree.Write()
    ftree.Close()


def saveplot(plt):
    plt.clf()
    plt.plot(data.x,data.y[0])
    plt.plot(data.x,data.y[1])
    plt.savefig("pixel_sipm.png")
    plt.show()
    plt.clf()
    plt.plot(data.x,data.y[2])
    plt.plot(data.x,data.y[3])
    plt.savefig("xpos_ypos.png")
    plt.clf()

if __name__ == "__main__":
    #parse command line
    flist =[]
    fout = "ftree.root"
    if (len(sys.argv)>1):
        fout = sys.argv[1]
        if (len(sys.argv)>2):
            flist = sys.argv[2:]
    if (len(flist)<1):
        print ("no file list given")
        quit()
    parse(fout,flist)
